# BigWigs

## [v31-classic](https://github.com/BigWigsMods/BigWigs/tree/v31-classic) (2021-09-16)
[Full Changelog](https://github.com/BigWigsMods/BigWigs/compare/v30.1-classic...v31-classic) [Previous Releases](https://github.com/BigWigsMods/BigWigs/releases)

- bump version  
- SSC/Lurker: Add Geyser, Fix phase changes  
- SSC/Lurker: Fix Spout trigger  
