## Interface: 11307
## Notes: World buff timers and pre warnings.
## Title: Nova World Buffs
## Author: Novaspark-Arugal OCE
## Version: 2.16
## SavedVariables: NWBdatabase

embeds.xml
Locale\Locales.xml
NovaWorldBuffs.lua
Options.lua
NWBHelp.lua
NWBData.lua
NWBCreatures.lua
NWBRealms.lua
NWBGuildFilter.lua