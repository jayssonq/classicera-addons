## Interface: 11302
## Title: TankWarningsClassic
## Version: 1.1.2
## Notes: Send chat warnings for critical tank cooldowns and/or failed ability attempts
## SavedVariables: TankWarningsClassicSV

TankWarningsClassic.xml